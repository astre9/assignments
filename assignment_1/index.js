
const FIRST_NAME = "ALEX";
const LAST_NAME = "STREZA";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) {
    var result = parseInt(value);
    
    if (value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER) {
        return NaN;
    }

    return result;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

