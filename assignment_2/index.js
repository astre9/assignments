
const FIRST_NAME = "ALEXANDRU";
const LAST_NAME = "STREZA";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};
    var obj = {};
    cache.pageAccessCounter = function (parameter) {
        //verific parametrul este null si daca exista deja proprietatea in obiect
        if (parameter == null && obj.hasOwnProperty('home'))
            obj['home'] = obj['home'] + 1; //incrementez valoarea proprietatii default
        else if (parameter == null && obj.hasOwnProperty('home') == false)
            obj['home'] = 1; // altfel initializez proprietatea default cu 1
        else {
            var pageName = String(parameter).toLowerCase();//convertesc la lowercase orice valoare data ca parametru
            if (obj.hasOwnProperty(pageName))//idem ca in cazul default
                obj[pageName] = obj[pageName] + 1;
            else
                obj[pageName] = 1;
        }
    };
    cache.getCache = function () {
        return obj;
    };
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

