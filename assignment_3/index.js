const FIRST_NAME = "Alexandru";
const LAST_NAME = "Streza";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
    getDetails() {
        return this.name + " " + this.surname + " " + this.salary;
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience) {
        super(name, surname, salary);
        if (experience != null)
            this.experience = experience;
        else
            this.experience = "JUNIOR";
    }
    applyBonus() {
        if (this.experience == "JUNIOR")
            return this.salary + this.salary * 0.1;
        else if (this.experience == "MIDDLE")
            return this.salary + this.salary * 0.15;
        else if (this.experience == "SENIOR")
            return this.salary + 0.2 * this.salary;
        else return this.salary * 0.1 + this.salary;
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

